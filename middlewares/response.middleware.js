const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    //next();
	if (res.err) {
		if (res.err.message == 'Fighter not found in database' || res.err.message == 'User not found') {
			res.status(404).send(JSON.stringify({error: true, message: res.err.message}));
		} else {
			res.status(404).send(JSON.stringify({error: true, message: 'Incorrect login credentials'}));
		}
	} else {
		res.status(200).send(JSON.stringify(res.data));
	}
}

exports.responseMiddleware = responseMiddleware;