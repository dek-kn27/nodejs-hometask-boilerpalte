const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
	if (!(req.body.hasOwnProperty('name')
		&& req.body.hasOwnProperty('power')
		&& req.body.hasOwnProperty('defense'))) {
		res.status(400).send(JSON.stringify({error: true, message: 'Please specify all parameters'}));
		return;
	}
	if (!(isFinite(req.body.power) && isFinite(req.body.defense) && req.body.power > 0 && req.body.defense > 0)) {
		res.status(400).send(JSON.stringify({error: true, message: 'Power and defense should be positive numbers'}));
		return;
	}
	if (req.body.power >= 100 || req.body.defense > 10) {
		res.status(400).send(JSON.stringify({error: true, message: 'Power should be a positive number less than 100, defense should be in between 1 and 10'}));
		return;
	}

    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    if (!(req.body.hasOwnProperty('name')
		&& req.body.hasOwnProperty('power')
		&& req.body.hasOwnProperty('defense'))) {
		res.status(400).send(JSON.stringify({error: true, message: 'Please specify all parameters'}));
		return;
	}
	if (!(isFinite(req.body.power) && isFinite(req.body.defense) && req.body.power > 0 && req.body.defense > 0)) {
		res.status(400).send(JSON.stringify({error: true, message: 'Power and defense should be positive numbers'}));
		return;
	}
	if (req.body.power >= 100 || req.body.defense > 10) {
		res.status(400).send(JSON.stringify({error: true, message: 'Power should be a positive number less than 100, defense should be in between 1 and 10'}));
		return;
	}

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;