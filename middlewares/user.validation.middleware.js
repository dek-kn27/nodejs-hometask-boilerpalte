const { user } = require('../models/user');

function validateEmail(email) {
	return email && email.length >= 11 && email.endsWith('@gmail.com');
}

function validatePassword(password) {
	return password && password.length >= 3;
}

function validatePhoneNumber(phoneNumber) {
	return /^\+380\d{9}$/.test(phoneNumber); // +380xxxxxxxxx
}

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
	if (validateEmail(req.body.email) && validatePassword(req.body.password) && req.body.firstName && req.body.lastName && validatePhoneNumber(req.body.phoneNumber)) {
		console.log("Validation successful");
		next();
	} else {
		console.log("Validation not successful");
		res.status(400).send(JSON.stringify({error: true, message: 'User entity to create is not valid'}));
	}
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;