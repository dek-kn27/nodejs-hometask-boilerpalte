const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
	getFighters() {
		return FighterRepository.getAll();
	}
	
	getFighterById(id) {
		const result = FighterRepository.getOne({id});
		if (result) {
			return result;
		} else {
			throw new Error('Fighter not found in database');
		}
	}
	
	addFighter({name, power, defense}) {
		return FighterRepository.create({name, power, defense, health: 100});
	}

	deleteFighter(id) {
		FighterRepository.delete(id);
	}
	
	updateFighter(id, {name, power, defense}) {
		const updateObject = {};
		if (name) updateObject.name = name;
		if (power) updateObject.power = power;
		if (defense) updateObject.defense = defense;
		
		if (!FighterRepository.update(id, updateObject)) {
			throw new Error('Fighter not found in database');
		}
	}
}

module.exports = new FighterService();