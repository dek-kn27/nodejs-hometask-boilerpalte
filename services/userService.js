const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
	add(user) {
		const userData = {email: user.email};
		
		if (this.search(userData)) {
			return false;
		}
		
		return UserRepository.create(user);
	}
	
	getAll() {
		return UserRepository.getAll();
	}

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
	
	delete(id) {
		UserRepository.delete(id);
	}
}

module.exports = new UserService();