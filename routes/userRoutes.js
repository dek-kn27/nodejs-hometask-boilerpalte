const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.post('', createUserValid, (req, res, next) => {
	console.log("test register");
    try {		
        const user = UserService.add(req.body);
		
		res.data = user;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('', (req, res, next) => {
    try {
        const user = UserService.getAll();
		
		res.data = user;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get(':id', (req, res, next) => {
    try {
        const user = UserService.search(req.params.id);
		if (!user) {
			throw new Error('User not found');
		}
		
		res.data = user;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete(':id', (req, res, next) => {
    try {
        UserService.delete(req.params.id);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;